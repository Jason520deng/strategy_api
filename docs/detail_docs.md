# strategy_api

### 介绍:
strategy_api 是关于量化策略的库，web3 大学推出的一款交易策略库，接通ex API，并且
打造策略框架，方便用户自定义实盘策略，更方便快捷的实现策略与APi的交互，目前接通交易
所：BINANCE、OKEX，包括U本位、币本位的api，

### strategy_api:

#### 安装教程
  
   1、源代码下载

    ```
    git clone git@gitee.com:Jason520deng/strategy_api.git
    ```

   2、包安装
  
    ```
    pip install strategy_api
    ```

#### [API文档](./api%20接口文档.md)
#### 使用说明
  
  1、Pycharm 安装下载
  
    https://www.jetbrains.com/pycharm/
  
  2、python 下载安装，python 安装 3.8 版本
  
    https://www.python.org/
  
  3、打开pycharm，安装strategy_api，还需要安装依赖包: aiohttp

    ```
    pip install aiohttp
    ```
  4、以上准备工作完成以后，正式开始介绍 strategy_api 的使用
  
  (1)、简易用法：单独使用API; 打开 pycharm, 创建文件 api_test.py(随意命名)

    ```
    # file： api_test.py

    import time

    # 导入BINANCE U本位 api包
    from strategy_api.tm_api.Binance.futureUsdt import BinanceFutureUsdtGateway

    # 导入BINANCE 币本位 api包
    from strategy_api.tm_api.Binance.futureInverse import BinanceFutureInverseGateway

    # 导入 OKEX 合约 api 包( 一个包包含了 U本位、币本位, 根据指标(symbol)的不同进行区别)
    from strategy_api.tm_api.Okex.futureUsdt import OkexFutureUsdtGateway

    # 导入 strategy_api 中使用的常量对象
    from strategy_api.tm_api.object import Interval, TickData, BarData, OrderData, DataType, PositionSide

    # 初始化事件引擎
    event_engine = EventEngine()

    # 初始化BINANCE 币本位 APi 网关
    binance_api = BinanceFutureInverseGateway(event_engine)

    # 定义 链接网关需要的参数
    api_setting = {
        "key": "",                # API key
        "secret": "",             # API 秘匙
        "proxy_host": "",         # 代理 地址 (不使用就不填)
        "proxy_port": 0,          # 代理 端口 (不使用代理就不填)
        "Passphrase": ""          # API 密码 (BINANCE 没有，OKEX 有)
    }

    # 链接 币安 API
    binance_api.connect(api_setting)

    # 链接 币安api 是协成的，所以这里需要等待 5 秒，保证api链接成功，才能进行api的调用
    time.sleep(5)

    # 订阅行情接口（websocket）
    binance_api.subscribe(symbol="BTCUSD_PERP", data_type=DataType.BAR, interval=Interval.MINUTE)

    # 下单接口 (rest http)
    my_order_id = binance_api.buy(symbol="BTCUSDT_PERP", volume=0.01, price=9999, maker=True, stop_loss=False, stop_profit=False)

    # 撤单接口
    binance_api.cancel_order(orderid=my_order_id, symbol="BTCUSDT")
    ```
  -----------------详细 API 介绍，请看 [API文档](./api%20接口文档.md)

  (2)、结合策略模版使用API; 打开pycharm， 创建文件 strategy_api.py (随意命名)

```
# 导入 strategy_api 包里面的策略模版
from strategy_api.strategies.template import StrategyTemplate
# 导入 BINANCE U 本位合约 API包
from strategy_api.tm_api.Okex.futureUsdt import OkexFutureUsdtGateway
# 导入 strategy_api 中使用的常量对象
from strategy_api.tm_api.object import Interval, BarData, OrderData, Status, PositionSide, DataType

import socket
import socks
proxy_host = "127.0.0.1"  # 代理地址
proxy_port = 1080  # 代理端口号
socks.set_default_proxy(socks.SOCKS5, proxy_host, proxy_port)
socket.socket = socks.socksocket

# 策略类
class StrategyDemo(StrategyTemplate):
    # 属性 作者(标志该策略的开发人员)
    author = "DYX"

    # 初始化方法
    def __init__(self):
        super(StrategyDemo, self).__init__()

    # 初始化策略参数
    def init_parameters(self):
        self.buy_switch = True
        self.long_id = ""
        self.stop_loss_id = ""
        self.stop_profit_id = ""
        self.volume = 1
        self.rate_stop = 0.002

    # k 线数据的回调, 可以在该方法里面记录 k 线数据、分析k线数据
    def on_bar(self, bar: BarData):
        okex_api = self.get_gateway("okex_api")
        # print(bar)
        if self.buy_switch:
            # 开多单
            self.long_id = okex_api.new_order_id()

            okex_api.buy(
                        orderid=self.long_id,
                        symbol="SOL-USD-SWAP",
                        volume=self.volume,
                        price=10
                        )

            self.buy_switch = False

    # 获取历史k线，获取最新一根k线的开盘价
    def query_history_kline(self):
        okex_api = self.get_gateway("okex_api")
        kls = okex_api.query_history(symbol="SOL-USD-SWAP", minutes=5, interval=Interval.MINUTE)
        open_price = kls[-1].open_price
        return open_price

    # 订单 数据的回调，订单状态的改变都会通过websoket 推送到这里，例如 从提交状态 改为 全成交状态，或者提交状态 改为 撤销状态 都会推送
    # 可以在这里对仓位进行一个记录
    def on_order(self, order: OrderData):
        okex_api = self.get_gateway("okex_api")

        if order.status == Status.ALLTRADED and self.long_id == order.orderid:
            open_price = self.query_history_kline()

            self.stop_profit_id = okex_api.new_order_id()
            okex_api.sell(
                          orderid=self.stop_profit_id,
                          symbol="SOL-USD-SWAP",
                          volume=self.volume,
                          price=round(open_price * (1 + self.rate_stop), 3),
                          stop_profit=True
                          )

            self.stop_loss_id = okex_api.new_order_id()
            okex_api.sell(
                          orderid=self.stop_loss_id,
                          symbol="SOL-USD-SWAP",
                          volume=self.volume,
                          price=round(open_price * (1 - self.rate_stop), 3),
                          stop_loss=True
                          )

        elif order.status == Status.ALLTRADED and (self.stop_profit_id == order.orderid or self.stop_loss_id == order.orderid):
            # okex 不用测小止损止盈单，只要止损了，止盈单自动撤销

            if self.stop_loss_id == order.orderid:
                pass
                # okex_api.cancel_order(orderid=self.stop_profit_id, symbol="SOL-USD-SWAP")
            elif self.stop_profit_id == order.orderid:
                pass
                # okex_api.cancel_order(orderid=self.stop_loss_id, symbol="SOL-USD-SWAP")

            self.buy_switch = True

def start_strategy(api_setting):
    # 初始化策略
    s = StrategyDemo()

    # 添加 BINANCE U本位网关
    okex_gateway = s.add_gateway(OkexFutureUsdtGateway, "okex_api", api_setting)

    # 订阅数据
    okex_gateway.subscribe(symbol="SOL-USD-SWAP", data_type=DataType.BAR, interval=Interval.MINUTE)

if __name__ == '__main__':
    print("启动量化系统: 等待策略运行")
    api_setting = {
        "key": "",
        "secret": "",
        "proxy_host": "",
        "proxy_port": 0,
        "Passphrase": ""
    }
    start_strategy(api_setting)
```

  -----------------详细 策略模版 介绍，请看 [策略模版文档](./结合策略模版介绍API文档.md)


#### [所有自定义类型对象、枚举](./object.md)
