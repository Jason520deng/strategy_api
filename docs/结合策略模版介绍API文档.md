## 策略模版 + API 说明文档

### 一、示例用法
```
# 导入策略模版
from strategy_api.strategies.template import StrategyTemplate

# 定义策略类并集成成该策略模版；这时候该策略类就拥有了模版的所有功能方法
class StrategyDemo(StrategyTemplate):
    
    def __init__(self):
        super(StrategyDemo, self).__init__() 
    
    # 新的tick数据更新回调。
    def on_tick(self, tick: TickData) -> None:
        pass

    # 新的bar数据更新回调。
    def on_bar(self, bar: BarData) -> None:
        pass

    # 新订单数据更新回调。
    def on_order(self, order: OrderData) -> None:
        pass
    
strategy = StrategyDemo()
```

### 二、功能介绍

- 使用策略模版，能更加简易的创建策略，该模版为用户集成了网关插槽、API调用、
  数据回调等功能，让用户只专心与策略逻辑的实现，而不用烦心于策略与API之间的调用关系。
  
#### 网关插槽

- 说明：用户可以在一个策略中链接多个网关
  
##### 1、插入网关 add_gateway: 

```angular2html
binanceAPI = strategy.add_gateway(gateway_class=BinanceFutureUsdtGateway, gateway_name = 'binance_api')
```  

######参数说明

参数名     | 类型      | 是否必须 | 描述
---       | :---     | :---    | :---
gateway_class | 对象(API对象)| 是 | API 对象, ex: BinanceFutureUsdtGateway、OkexFutureUsdtGateway
gateway_name | str | 是 | 网关自定义名称，为该网关取名，后面取出网关会用到

######返回结果：网关实例对象,可直接调用[API相关方法](./api%20接口文档.md)

##### 2、获取网关 get_gateway:

```angular2html
binanceAPI = strategy.get_gateway(gateway_name = 'binance_api')
```  

######参数说明

参数名     | 类型      | 是否必须 | 描述
---       | :---     | :---    | :---
gateway_name | str | 是 | 网关自定义名称，用户之前自定义的名称

######返回结果：网关实例对象,可直接调用[API相关方法](./api%20接口文档.md)

#### API调用

- 说明: 用户使用API 实例，直接调用[API相关方法](./api%20接口文档.md)

```angular2html
binanceAPI = strategy.get_gateway(gateway_name = 'binance_api')
binanceAPI.subscribe(symbol="BTCUSDT", data_type=DataType.BAR, interval=Interval.MINUTE)
```  

#### 数据回调

- 说明：为用户集成了订单数据回调、K线数据回调、tick数据回调 等三个方法

##### *(1)订单数据回调*

```
def on_order(self, bar: OrderData) -> None:
    # 在这里可以处理 回调过来的 订单数据: OrderData
    pass
```

##### *(2)k线数据回调*

```
def on_bar(self, tick: BarData) -> None:
    # 在这里可以处理 回调过来的 K线数据: BarData
    pass
```

##### *(3)tick数据回调*

```
def on_tick(self, bar: TickData) -> None:
    # 在这里可以处理 回调过来的 tick数据: TickData
    pass
```

#### 相关数据结构 OrderData、BarData、TickData

- OrderData : K线数据结构

方法     | 说明 
---       | :---
OrderData.symbol | 订单标的
OrderData.orderid | 订单号
OrderData.status | 订单状态



- BarData : K线数据结构

方法     | 说明 
---       | :---
BarData.symbol| K 线标的
BarData.datetime|  K线开始时间
BarData.endTime|  K线结束时间
BarData.interval|  K线间隔
BarData.volume |  K线交易量
BarData.turnover|  K线交易量(U)
BarData.open_price|  K线开盘价
BarData.high_price|  K线最高价
BarData.low_price | K线最低价
BarData.close_price| K线收盘价
BarData.exchange |  K线来源交易所

- TickData: tick数据对象

方法     | 说明 
---       | :---
TickData.symbol | 标的
TickData.datetime | 该数据时间
TickData.last_price | tick 最新价

