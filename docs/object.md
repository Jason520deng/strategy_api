### DataType : 行情数据类型

属性方法     | 属性类型 | 说明 
---       | :--- | :---
DataType.TICK | str |tick 数据类型
DataType.BAR | str  |K线数据类型

### Interval : K线数据间隔

属性方法     | 属性类型 | 说明 
---       | :--- | :---
Interval.MINUTE| str | 一分钟间隔(k线)
Interval.MINUTE_3| str | 三分钟间隔(k线)
Interval.MINUTE_5| str | 五分钟间隔(k线)
Interval.MINUTE_15| str| 十五分钟间隔(k线)
Interval.MINUTE_30|str | 三十分钟间隔(k线)
Interval.HOUR|str | 一小时间隔(k线)
Interval.HOUR_2|str |两小时间隔(k线)
Interval.HOUR_4|str |四小时间隔(k线)
Interval.HOUR_6|str |六小时间隔(k线)
Interval.HOUR_8|str |八小时间隔(k线)
Interval.HOUR_12|str |十二小时间隔(k线)

### Status : 订单状态

属性方法     | 属性类型 | 说明 
---       | :--- | :---
Status.SUBMITTING| str | 订单提交中
Status.NOTTRADED| str  | 订单未成交
Status.PARTTRADED| str | 订单部分成交
Status.ALLTRADED| str | 订单全部成交
Status.CANCELLED| str | 订单被撤销
Status.REJECTED| str | 订单被拒绝

### OrderType : 订单类型

属性方法     | 属性类型 | 说明 
---       | :--- | :---
OrderType.LIMIT| str | 限价单
OrderType.MARKET| str | 市价单
OrderType.STOP_MARKET| str | 止损单
OrderType.TAKE_PROFIT_MARKET| str | 止盈单
OrderType.STOP_LOSS_PROFIT| str | 止盈止损单

### PositionSide : 仓位模式类型

属性方法     | 属性类型 | 说明 
---       | :--- | :---
PositionSide.ONEWAY| str | 单向持仓
PositionSide.TWOWAY| str | 双向持仓

### Exchange : 交易所类型

属性方法     | 属性类型 | 说明 
---       | :--- | :---
Exchange.BINANCE | str | 币安
Exchange.OKEX | str | oke

### BarData : K线数据

属性方法   | 属性类型 | 说明 
---    |  :--- | :---
BarData.symbol| str |K 线标的
BarData.datetime|datetime | K线开始时间
BarData.endTime| datetime |K线结束时间
BarData.interval| Interval |K线间隔
BarData.volume | float |K线交易量
BarData.turnover| float |K线交易量(U)
BarData.open_price| float |K线开盘价
BarData.high_price| float |K线最高价
BarData.low_price | float |K线最低价
BarData.close_price| float |K线收盘价
BarData.exchange | Exchange |K线来源交易所


### TickData : tick 数据

属性方法   | 属性类型 | 说明 
---    |  :--- | :---
TickData.symbol|  str | 标的
TickData.datetime | datetime | 该tick数据时间
TickData.last_price| float | tick 最新价
TickData.exchange | Exchange |tick线来源交易所

### OrderData : 订单数据

属性方法   | 属性类型 | 说明 
---       | :--- |:---
OrderData.symbol| str | 订单标的
OrderData.orderid | str | 订单号
OrderData.status | Status |订单状态
OrderData.exchange | Exchange |订单数据来源交易所
OrderData.traded | float | 订单累计成交量
OrderData.traded_price | float | 订单末次成交价
OrderData.traded_volume | float | 订单末次成交量 

### ToMode : 交易模式

属性方法     | 属性类型 | 说明 
---       | :--- | :---
ToMode.CROSS | str | 全仓
ToMode.ISOLATED | str | 逐仓

### Chain : 提币使用公链

属性方法 | 属性类型 | 说明
--- | :--- | :---
Chain.TRC20| str | 波场公链
Chain.ERC20 | str |  以太公链
Chain.OKC | str |  OKC 公链
Chain.BNB_SMART_CHAIN | str | 币安智能链
Chain.BNB_BEACON_CHAIN | str | 币安信标链

### Dest : 转账方法

属性方法 | 属性类型 | 说明
--- | :--- | :---
Dest.CHAIN| str | 链上转账
Dest.INTERNAL | str |  内部转账


### DepthData ：深度数据

属性方法 | 属性类型 | 说明
--- | :--- | :---
DepthData.symbol| str | 标的
DepthData.bid_data | BidData |  买盘数据 
DepthData.ask_data | AskData |  卖盘数据

### BidData ：买盘数据

属性方法 | 属性类型 | 说明
--- | :--- | :---
BidData.bid_price | float |  价格 
BidData.bid_volume | float |  数量

### BidData ：卖盘数据

属性方法 | 属性类型 | 说明
--- | :--- | :---
BidData.ask_price | float |  价格 
BidData.ask_volume | float |  数量

### PositionData ：仓位数据

属性方法 | 属性类型 | 说明
--- | :--- | :---
PositionData.exchange | Exchange |  交易所 
PositionData.symbol | str |  交易对
PositionData.volume | float |  头寸数量, 符号代表多空方向, 正数为多, 负数为空
PositionData.price | float |  开仓均价
PositionData.mark_price | float |  当前标记价格
PositionData.liquidation_price | float |  参考强平价格
PositionData.leverage | int |  当前杠杆倍数
PositionData.pnl | float |  持仓未实现盈亏