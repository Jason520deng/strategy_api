```
示例：
from strategy_api.event.engine import EventEngine
from strategy_api.tm_api.Binance.spotGateway import BinanceSpotGateway
event_engine = EventEngine()
api = BinanceSpotGateway(event_engine)
```
### EventEngine 事件引擎说明

- 主要功能: 注册 API 结果的回调方法(K线数据回调、tick数据回调、订单数据回调)，请看[详系介绍](../EVENTENGINE.md)

### BinanceFutureUsdtGateway API 说明

### 一、REST API

#### 1、交易

##### (1).下单(做多)

- api.buy(symbol="BTC-USDT-SPOT", volume=0.01, price=9999, maker=True, stop_loss=False, stop_profit=False, position_side=PositionSide.ONEWAY)

######调用参数：
参数名     | 类型      | 是否必须 | 描述
---       | :---     | :---    | :---
orderid | str | 是 | 订单ID
symbol    | str      |  是     | 标的
volume | float |  是     | 下单数量
price  | float |  是     | 下单价格(当下止损止盈单时，该价格为执行价格)
maker | bool | 否  | 下单类型， Ture 为限价单，False 为市价单，默认为False
stop_loss | bool | 否 |是否止损单， Ture 为止损单，False 为忽略，默认为False
stop_loss_price | float | 否 | 止损触发价，当stop_loss 为 True时必填
stop_profit |bool | 否 |是否止盈单， Ture 为止损单，False 忽略，默认为False
stop_profit_price | float | 否 | 止盈触发价，当stop_profit 为True 时必填
position_side | PositionSide | 否 | 仓位模式（现货杠杆不使用该参数），PositionSide.ONEWAY: 单仓模式，PositionSide.TWOWAY: 双仓模式，默认为单仓模式 
tdMode | TdMode | 否 |交易模式，杠杆交易必填， TdMode.CROSS(全仓)， TdMode.ISOLATED(逐仓)

######返回结果：自定义订单号；类型为str

######是否数据回调：订单回调

##### (2).下单(做空)

- api.short(symbol="BTC-USDT-SPOT", volume=0.01, price=9999, maker=True, stop_loss=False, stop_profit=False, position_side=PositionSide.ONEWAY)
######调用参数：

参数名     | 类型      | 是否必须 | 描述
---       | :---     | :---    | :---
orderid | str | 是 | 订单ID
symbol    | str      |  是     | 标的
volume | float |  是     | 下单数量
price  | float |  是     | 下单价格(当下止损止盈单时，该价格为执行价格)
maker | bool | 否  | 下单类型， Ture 为限价单，False 为市价单，默认为False
stop_loss | bool | 否 |是否止损单， Ture 为止损单，False 为忽略，默认为False
stop_loss_price | float | 否 | 止损触发价，当stop_loss 为 True时必填
stop_profit |bool | 否 |是否止盈单， Ture 为止损单，False 忽略，默认为False
stop_profit_price | float | 否 | 止盈触发价，当stop_profit 为True 时必填
position_side | PositionSide | 否 | 仓位模式（现货杠杆不使用该参数），PositionSide.ONEWAY: 单仓模式，PositionSide.TWOWAY: 双仓模式，默认为单仓模式 
tdMode | TdMode | 否 |交易模式，杠杆交易必填， TdMode.CROSS(全仓)， TdMode.ISOLATED(逐仓)
######返回结果：自定义订单号；类型为str

######是否数据回调：订单回调

##### (3).下单(平多)

- api.sell(symbol="BTC-USDT-SPOT", volume=0.01, price=9999, maker=True, stop_loss=False, stop_profit=False, position_side=PositionSide.ONEWAY)
######调用参数：

参数名     | 类型      | 是否必须 | 描述
---       | :---     | :---    | :---
orderid | str | 是 | 订单ID
symbol    | str      |  是     | 标的
volume | float |  是     | 下单数量
price  | float |  是     | 下单价格(当下止损止盈单时，该价格为执行价格)
maker | bool | 否  | 下单类型， Ture 为限价单，False 为市价单，默认为False
stop_loss | bool | 否 |是否止损单， Ture 为止损单，False 为忽略，默认为False
stop_loss_price | float | 否 | 止损触发价，当stop_loss 为 True时必填
stop_profit |bool | 否 |是否止盈单， Ture 为止损单，False 忽略，默认为False
stop_profit_price | float | 否 | 止盈触发价，当stop_profit 为True 时必填
position_side | PositionSide | 否 | 仓位模式（现货杠杆不使用该参数），PositionSide.ONEWAY: 单仓模式，PositionSide.TWOWAY: 双仓模式，默认为单仓模式 
tdMode | TdMode | 否 |交易模式，杠杆交易必填， TdMode.CROSS(全仓)， TdMode.ISOLATED(逐仓)

######返回结果：自定义订单号；类型为str

######是否数据回调：订单回调

##### (4).下单(平空)

- api.cover(symbol="BTC-USDT-SPOT", volume=0.01, price=9999, maker=True, stop_loss=False, stop_profit=False, position_side=PositionSide.ONEWAY)

######调用参数：
参数名     | 类型      | 是否必须 | 描述
---       | :---     | :---    | :---
orderid | str | 是 | 订单ID
symbol    | str      |  是     | 标的
volume | float |  是     | 下单数量
price  | float |  是     | 下单价格(当下止损止盈单时，该价格为执行价格)
maker | bool | 否  | 下单类型， Ture 为限价单，False 为市价单，默认为False
stop_loss | bool | 否 |是否止损单， Ture 为止损单，False 为忽略，默认为False
stop_loss_price | float | 否 | 止损触发价，当stop_loss 为 True时必填
stop_profit |bool | 否 |是否止盈单， Ture 为止损单，False 忽略，默认为False
stop_profit_price | float | 否 | 止盈触发价，当stop_profit 为True 时必填
position_side | PositionSide | 否 | 仓位模式（现货杠杆不使用该参数），PositionSide.ONEWAY: 单仓模式，PositionSide.TWOWAY: 双仓模式，默认为单仓模式 
tdMode | TdMode | 否 |交易模式，杠杆交易必填， TdMode.CROSS(全仓)， TdMode.ISOLATED(逐仓)

######返回结果：自定义订单号；类型为str

######是否数据回调：订单回调

##### (5).撤单

- api.cancel_order(orderid="xl_1111111", symbol="BTC-USDT-SPOT")

######调用参数：
参数名     | 类型      | 是否必须 | 描述
---       | :---     | :---    | :---
orderid | str | 是 | 订单号
symbol | str | 是 | 标的

######返回结果：无

######是否数据回调：订单回调

##### (6).撤销某标的的全部挂单

- api.cancel_all_order(symbol="BTC-USDT-SPOT")

######调用参数：
参数名     | 类型      | 是否必须 | 描述
---       | :---     | :---    | :---
symbol | str | 是 | 标的

######返回结果：bool

######是否数据回调：订单回调

##### (7).获取订单信息 ：根据标的和订单号获取

- order = api.query_order(symbol = "BTC-USDT-SPOT", orderId = '5463494692529682458')
######调用参数：
参数名     | 类型      | 是否必须 | 描述
---       | :---     | :---    | :---
orderId | str | 是 | 订单号
symbol | str | 是 | 标的

######返回结果：OrderData

##### (8).获取未成交订单列表信息 ：根据标的获取

- order = api.query_orders(symbol = "BTC-USDT")
######调用参数：
参数名     | 类型      | 是否必须 | 描述
---       | :---     | :---    | :---
symbol | str | 是 | 标的

######返回结果：List[OrderData]


#### 2、行情数据

##### 获取单个标的k线数据

- api.query_history(symbol="BTC-USDT-SPOT", interval=Interval.MINUTE, minutes=20, hour=2, end_time=datetime.today())

######调用参数：
参数名     | 类型      | 是否必须 | 描述
---       | :---     | :---    | :---
  symbol |str | 是 | 标的
interval | Interval | 是 | 间隔
minutes | int | 否 (minutes 与 hour 必须有一个必传) | 分钟数，查询前几分钟
hour | int | 否 (minutes 与 hour 必须有一个必传) | 小时数，查询前几小时
end_time | datetime| 否 | K线结束时间，不传默认为当前时间 结束

######返回结果：k线数据，类型为 BarData

######是否数据回调：否


##### 深度信息(买盘卖盘)

- api.query_depth(symbol="BTC-USDT-SPOT", limit=10)

######调用参数：
参数名     | 类型      | 是否必须 | 描述
---       | :---     | :---    | :---
symbol |str | 是 | 标的
limit | int | 是 | 档位，可选值：[5, 10, 20, 50, 100, 500, 1000]

######返回结果：买盘卖盘数据，类型为 DepthData

##### 产品信息(币种)

- api.query_symbol(quote_symbol="USDT", s_type="SPOT")

######调用参数：
参数名     | 类型      | 是否必须 | 描述
---       | :---     | :---    | :---
quote_symbol |str | 是 | 计价币
s_type | str | 是 | 币种类型 (SPOT\MARGIN\""\PERP)

######返回结果：list 

#### 3、资产

##### (1).提币

- api.withdraw_coin(symbol="USDT", amount=10,
                    toAddress="TUenpq7W51pUyeZNMSz9xeHJQKHGGpUGN4", chain=Chain.TRC20)

######调用参数：
参数名     | 类型      | 是否必须 | 描述
---       | :---     | :---    | :---
symbol |str | 是 | 币种
amount | float | 是 | 数量
toAddress | str | 是 | 转账地址
chain | Chain | 是 | 使用公链

######返回结果：bool

### 二、WebSocket API

#### 1、行情数据

- order_id = api.subscribe(symbol="BTC-USDT-SPOT", data_type=DataType.BAR, interval=Interval.MINUTE)

######调用参数：
参数名     | 类型      | 是否必须 | 描述                
---       | :---     | :---    | :---                       
symbol    | str      |  是     | 标的                 
data_type | DataType |  是     | 订阅行情类型
interval  | Interval |  否     | k线间隔(订阅K线时，必填)

######返回结果：无

######是否数据回调：K线数据回调、tick数据回调


### 三、链接API 参数说明

```angular2html
   api_setting = {
        "key": "",  # api key
        "secret": "",  # api 密钥
        "proxy_host": "",           # 代理地址
        "proxy_port": 0,            # 代理端口
        "Passphrase": "",  # api secret_key 的密码

        "call_order_switch": False,  # 订单数据回调 开关
        "call_tick_switch": False,  # tick数据回调 开关
        "call_bar_switch": False  # k线数据回调 开关
    }
```

### 四、API 特殊类型说明

- Interval  ： K 线间隔
  
方法     | 说明 
---       | :---
Interval.MINUTE | 一分钟间隔(k线)
Interval.MINUTE_3 | 三分钟间隔(k线)
Interval.MINUTE_5 | 五分钟间隔(k线)
Interval.MINUTE_15 | 十五分钟间隔(k线)
Interval.MINUTE_30 | 三十分钟间隔(k线)
Interval.HOUR | 一小时间隔(k线)
Interval.HOUR_2 |两小时间隔(k线)
Interval.HOUR_4 |四小时间隔(k线)
Interval.HOUR_6 |六小时间隔(k线)
Interval.HOUR_8 |八小时间隔(k线)
Interval.HOUR_12 |十二小时间隔(k线)

- PositionSide ： 持仓模式
  
方法     | 说明 
---       | :---
PositionSide.ONEWAY | 单向持仓
PositionSide.TWOWAY | 双向持仓

- DataType  : 订阅数据类型
  
方法     | 说明 
---       | :---
DataType.TICK | tick 数据类型
DataType.BAR |  K线数据类型

- BarData : K线数据结构

方法     | 说明 
---       | :---
BarData.symbol| K 线标的
BarData.datetime|  K线开始时间
BarData.endTime|  K线结束时间
BarData.interval|  K线间隔
BarData.volume |  K线交易量
BarData.turnover|  K线交易量(U)
BarData.open_price|  K线开盘价
BarData.high_price|  K线最高价
BarData.low_price | K线最低价
BarData.close_price| K线收盘价
BarData.exchange |  K线来源交易所

- Exchange : 交易所类型

属性方法     | 属性类型 | 说明 
---       | :--- | :---
Exchange.BINANCE | str | 币安
Exchange.OKEX | str | oke

- TdMode : 交易模式

属性方法 | 属性类型 | 说明
--- | :--- | :---
TdMode.CROSS | str | 全仓
TdMode.ISOLATED | str | 逐仓

- Chain : 提币使用公链

属性方法 | 属性类型 | 说明
--- | :--- | :---
Chain.TRC20| str | 波场公链
Chain.ERC20 | str |  以太公链
Chain.OKC | str |  OKC 公链
Chain.BNB_SMART_CHAIN | str | 币安智能链
Chain.BNB_BEACON_CHAIN | str | 币安信标链

- DepthData ：深度数据

属性方法 | 属性类型 | 说明
--- | :--- | :---
DepthData.symbol| str | 标的
DepthData.bid_data | BidData |  买盘数据 
DepthData.ask_data | AskData |  卖盘数据

- BidData ：买盘数据

属性方法 | 属性类型 | 说明
--- | :--- | :---
BidData.bid_price | float |  价格 
BidData.bid_volume | float |  数量

- BidData ：卖盘数据

属性方法 | 属性类型 | 说明
--- | :--- | :---
BidData.ask_price | float |  价格 
BidData.ask_volume | float |  数量

-  OrderData : 订单数据

属性方法   | 属性类型 | 说明 
---       | :--- |:---
OrderData.symbol| str | 订单标的
OrderData.orderid | str | 订单号
OrderData.status | Status |订单状态
OrderData.exchange | Exchange |订单数据来源交易所
OrderData.traded | float | 订单累计成交量
OrderData.traded_price | float | 订单末次成交价
OrderData.traded_volume | float | 订单末次成交量 
