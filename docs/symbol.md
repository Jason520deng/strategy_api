## 统一标的规则

### U本为标的(symbol)
BTC-USDT、ETH-USDT、...

### 币本位标的(symbol)
BTC-USD-PERP、ETH-USD-PERP、...

### 杠杆标的(symbol)
BTC-USDT-MARGIN

### 现货标的(symbol)
BTC-USDT-SPOT