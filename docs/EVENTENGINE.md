### 完整示例
```angular2html
from strategy_api.event.engine import Event, EventEngine, EVENT_TICK, EVENT_BAR, EVENT_ORDER

# 定义 tick 数据回调方法 
def process_tick_event(event: Event):
    tick: TickData = event.data
    print("tick 数据：")
    print(tick)

# 定义 K线 数据回调方法
def process_bar_event(event: Event):
    bar: BarData = event.data
    print("k 线 数据：")
    print(bar)

# 定义 订单数据 回调方法
def process_order_event(event: Event):
    order: OrderData = event.data
    print("订单 数据：")
    print(order)

event_engine = EventEngine()

# 事件引擎开始
event_engine.start()
# 注册 tick 数据回调方法
event_engine.register(EVENT_TICK, process_tick_event)
# 注册 K线 数据回调方法
event_engine.register(EVENT_BAR, process_bar_event)
# 注册 订单 数据回调方法
event_engine.register(EVENT_ORDER, process_order_event)
```

### 事件引擎接口
#### 1、启动事件引擎
- event_engine.start()

#### 2、注册回调方法
- event_engine.register(type = EVENT_TICK, handler=process_tick_event)

####### 调用参数

参数名     | 类型      | 是否必须 | 描述
---       | :---     | :---    | :---
type  | str | 是 | 数据类型， 注册回调什么数据的方法，规定了三种数据类型：EVENT_TICK、EVENT_BAR、EVENT_ORDER
handler | function | 是 | 回调方法

####### 返回结果：无

### 定义回调方法说明
- def process_tick_event(event: Event):

####### 定义参数
参数名     | 类型      | 是否必须 | 描述
---       | :---     | :---    | :---
event | Event | 是 | 事件数据对象，回调的数据就在这个参数里面 

### 特殊类型说明
- Event：事件数据对象

方法     | 说明 
---       | :---
Event.type | 数据类型
Event.data | 数据内容

- TickData: tick数据对象


属性方法   | 属性类型 | 说明 
---    |  :--- | :---
TickData.symbol|  str | 标的
TickData.datetime | datetime | 该tick数据时间
TickData.last_price| float | tick 最新价
TickData.exchange | Exchange |tick线来源交易所


- BarData: k线数据对象

属性方法   | 属性类型 | 说明 
---    |  :--- | :---
BarData.symbol| str |K 线标的
BarData.datetime|datetime | K线开始时间
BarData.endTime| datetime |K线结束时间
BarData.interval| Interval |K线间隔
BarData.volume | float |K线交易量
BarData.turnover| float |K线交易量(U)
BarData.open_price| float |K线开盘价
BarData.high_price| float |K线最高价
BarData.low_price | float |K线最低价
BarData.close_price| float |K线收盘价
BarData.exchange | Exchange |K线来源交易所

- OrderData: 订单数据对象

属性方法   | 属性类型 | 说明 
---       | :--- | :---
OrderData.symbol| str | 订单标的
OrderData.orderid | str | 订单号
OrderData.status | Status |订单状态
OrderData.exchange | Exchange |订单数据来源交易所
OrderData.traded | float | 订单累计成交量
OrderData.traded_price | float | 订单末次成交价
OrderData.traded_volume | float | 订单末次成交量 
