# strategy_api

### 介绍:
strategy_api 是关于量化策略的库，web3 大学推出的一款交易策略库，接通ex API，并且
打造策略框架，方便用户自定义实盘策略，更方便快捷的实现策略与APi的交互，目前接通交易
所：BINANCE、OKEX，包括U本位、币本位的api，

### [strategy_api 详细文档](./docs/detail_docs.md)
